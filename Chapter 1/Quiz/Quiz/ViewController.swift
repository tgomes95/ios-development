//
//  ViewController.swift
//  Quiz
//
//  Created by Thi on 5/14/16.
//  Copyright © 2016 tgomes95. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    let questions: [String] = ["From what is cognac made?",
                               "What is 7 + 7?",
                               "What is the capital of Vermont?"]
    
    let answers: [String] = ["Grapes", "14", "Montpelier"]
    
    var currentQuestionIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLabel.text = questions[currentQuestionIndex]
    }
    
    @IBAction func showNextQuestion(sender: UIButton) {
        currentQuestionIndex += 1
        
        let question: String = questions[currentQuestionIndex % 3]
        
        questionLabel.text = question
        answerLabel.text = "?"
    }
    
    
    @IBAction func showAnswer(sender: UIButton) {
        let answer: String = answers[currentQuestionIndex % 3]
        answerLabel.text = answer
    }

}


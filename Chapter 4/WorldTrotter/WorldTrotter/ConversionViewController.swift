//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Thi on 5/19/16.
//  Copyright © 2016 tgomes95. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController {
    
    @IBOutlet var celsiusLabel: UILabel!
   
    @IBOutlet var textField: UITextField!
    
    @IBAction func fahrenheitFieldEditingChanged(sender: UITextField) {
        
        if let text = sender.text where !text.isEmpty {
            celsiusLabel.text = sender.text
        }
        else {
            celsiusLabel.text = "?"
        }
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        textField.resignFirstResponder()
    }
}